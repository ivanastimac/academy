let stringMutable = '';
const stringImmutable = ' neki string';
let numberMutable = 5.52;
const numberImmutable = 10;
let booleanMutable = true;
const booleanImmutable = false;
let nullVariable = null;
const undefinedVariable = undefined;

console.log("Section 1");
console.log("1 - " + stringMutable);
console.log("2 - " + stringImmutable);
console.log("3 - " + numberMutable);
console.log("4 - " + numberImmutable);
console.log("5 - " + booleanMutable);
console.log("6 - " + booleanImmutable);
console.log("7 - " + nullVariable);
console.log("8 - " + undefinedVariable);

console.log("\n");
console.log("Section 2");
console.log(typeof stringMutable);
console.log(typeof stringImmutable);
console.log(typeof numberMutable);
console.log(typeof numberImmutable);
console.log(typeof booleanMutable);
console.log(typeof booleanImmutable);
console.log(typeof nullVariable);
console.log(typeof undefinedVariable);

console.log("\n");
console.log("Section 3");
const resultOfSum = 4 + 5;
const resultOfSubtraction = 5 - 3;
const resultOfMultiplication = 4 * 4;
const resultOfDivision = 10 / 2;

console.log(resultOfSum);
console.log(resultOfSubtraction);
console.log(resultOfMultiplication);
console.log(resultOfDivision);

console.log("\n");
console.log("Section 4");
stringMutable = "Galatasaray is my favourite club";
console.log(stringMutable + "" + stringImmutable);

console.log("\n");
console.log("Section 5");
numberToString1 = numberMutable.toString();
numberToString2 = numberImmutable.toString();
console.log(numberToString1);
console.log(numberToString2);

console.log(nullVariable == undefinedVariable);
console.log(nullVariable === undefinedVariable);