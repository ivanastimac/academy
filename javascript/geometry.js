class Shape {

    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    perimeter() {
        return undefined;
    }

    area() {
        return undefined;
    }

}

class Rectangle extends Shape {

    constructor(x, y) {
        super(x, y);
    }

    perimeter() {
        return 2 * this.x + 2 * this.y;
    }

    area() {
        return this.x * this.y;
    }
}

class Square extends Shape {

    constructor(x) {
        super(x, null);
    }

    perimeter() {
        return 4 * this.x;
    }

    area() {
        return this.x * this.x;
    }
}

class Circle extends Shape {

    constructor(r) {
        super(null, null);
        this.r = r;
    }

    perimeter() {
        return 2 * this.r * Math.PI;
    }

    area() {
        return this.r * this.r * Math.PI;
    }

}

const rectangle = new Rectangle(2, 3);
console.log("Perimeter of rectangle -> " + rectangle.perimeter());
console.log("Area of rectangle -> " + rectangle.area());

const square = new Square(3);
console.log("Perimeter of square -> " + square.perimeter());
console.log("Area of square -> " + square.area());

const circle = new Circle(2);
console.log("Perimeter of circle -> " + circle.perimeter());
console.log("Area of circle -> " + circle.area());
