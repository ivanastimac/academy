function factorial(number) {
    for (i = 1; i <= number; ++i) {
        result *= i;
    }
    return result;
}

const userInfo = {
    name: "Ivana",
    birthdate: new Date(1999, 7, 12),
    gender: "female",
    hasAPet: true,
    numberOfCountriesVisited: 10
}

const cloneOne = {...userInfo};

const cloneNamedDifferent = {...userInfo};
cloneNamedDifferent.name = "Ivan";

console.log(cloneOne === userInfo);

const address = {
    country: "Croatia",
    city: "Rijeka"
}

userInfo.address = address;
cloneNamedDifferent.address = address;

console.log(userInfo === cloneNamedDifferent);

function deleteField(arr, str) {
    const newArr = [];
    for (i = 0; i < arr.length; ++i) {
        const newObj = {...arr[i]};
        delete newObj[str];
        newArr.push(newObj);
    }
    return newArr;
}

const newArr = deleteField([userInfo, cloneOne], 'name');

console.log(newArr);

function Rectangle(a, b) {
    this.length = a;
    this.width = b;
    this.perimeter = 2 * (a + b);
    this.area = a * b;
}

rect = new Rectangle(3, 4);
console.log(rect);

function getMaxAndMin(numbers) {
    min = 100;
    max = 0;

    numbers.forEach(function(element) {
        if (element > max) {
            max = element;
        }
        if (element < min) {
            min = element;
        }
    });

    return {min, max};
}

obj = getMaxAndMin([2, 30, 4, 5, 2, 10]);
console.log(obj);
