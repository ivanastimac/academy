import axios from "axios";

class WeatherInfo {

    constructor(city) {
        this.city = city;
    }

    async getForecast() {
        let promise = axios.get("http://api.weatherapi.com/v1/current.json?key=1e91d93e064143ee95d130214210408&q=" + this.city + "&aqi=no");
        let result = await promise.catch(err => console.log(err));
        console.log(result.data);
    }

}

const infoRijeka = new WeatherInfo("Rijeka");
infoRijeka.getForecast();

const infoParis = new WeatherInfo("Paris");
infoParis.getForecast();

